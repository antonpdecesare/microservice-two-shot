from django.contrib import admin
from .models import LocationVO, Hat


admin.site.register(Hat)
admin.site.register(LocationVO)
