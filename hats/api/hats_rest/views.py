from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "picture_url",
        "location",
        "id"
        ]
    encoders = {
            "location": LocationVOEncoder()
        }

@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
            location_href= content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
            )

        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def detail_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder= HatListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
