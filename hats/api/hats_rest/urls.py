from .views import list_hats, detail_hats
from django.urls import path

urlpatterns = [
    path("hats/", list_hats, name="list_hats"),
    path("locations/<int:location_vo_id>/hats/", list_hats, name="list_hats"),
    path("hats/<int:pk>/", detail_hats, name="detail_hats"),
]
