import React from "react";


function HatsList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={ hat.id }>
                            <td>{ hat.style }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.color }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;
