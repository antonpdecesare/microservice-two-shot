import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));


async function loadShoes() {
  const response = await fetch('http://localhost:8080/api/shoes/');
  if (response.ok) {
    const shoesdata = await response.json();
    const response1 = await fetch('http://localhost:8090/api/hats/');
    if(response1.ok){
      const hatsdata = await response1.json();
    root.render(
      <React.StrictMode>
        <App shoes={shoesdata.shoes} hats={hatsdata.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
}
loadShoes();
