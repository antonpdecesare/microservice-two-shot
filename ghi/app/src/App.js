import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import HatsForm from './HatForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatsList hats={props.hats} />} />
          <Route path="hats">
            <Route path="new" element={<HatsForm />} />
          </Route>
          <Route path="shoes" element={<ShoesList shoes = {props.shoes} />} />
          <Route path="shoes">
            <Route path="new" element={<ShoesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
