from django.contrib import admin
from django.urls import path
from .views import api_list_shoes, detail_shoes
urlpatterns = [
    path("shoes/", api_list_shoes, name=("api_list_shoes")),
    path("bins/<int:bin_vo_id>/shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", detail_shoes, name="detail_shoes"),
]
